#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

#include "..\MYSTL\MyVector.hpp"

TEST_CASE("Default constructor of vector", "[Construction]") {
    
    MyVector<int> myVec;
    REQUIRE(myVec.Size() == 0);
    REQUIRE(myVec.Capacity() == 0);
 }

TEST_CASE("Parameterized constructor of vector", "[Construction]") {

    MyVector<int> myVec(9);
    REQUIRE(myVec.Size() == 9);
}

TEST_CASE("Initilizer list constructor of vector", "[Construction]") {

    MyVector<int> myVec{11, 22, 33, 44, 55, 66, 77, 88, 99};
    REQUIRE(myVec.Size() == 9);

    for (size_t i = 0; i < myVec.Size(); i++)
    {
        REQUIRE(myVec[i] ==  11 * (i + 1));
    }
}

TEST_CASE("Copy constructor of vector", "[Construction]") {

    MyVector<int> myVec{ 11, 22, 33, 44, 55, 66, 77, 88, 99 };
    REQUIRE(myVec.Size() == 9);

    MyVector<int> myVecCopy(myVec);
    for (size_t i = 0; i < myVec.Size(); i++)
    {
        REQUIRE(myVec[i] == 11 * (i + 1));
    }
}

TEST_CASE("Move constructor of vector", "[Construction]") {

    MyVector<int> myVec{ 11, 22, 33, 44, 55, 66, 77, 88, 99 };
    REQUIRE(myVec.Size() == 9);

    MyVector<int> myVecCopy(5);

    myVecCopy = std::move(myVec);
    for (size_t i = 0; i < myVec.Size(); i++)
    {
        REQUIRE(myVecCopy[i] == 11 * (i + 1));
    }
}


TEST_CASE("Assignment of vector", "[Assignment]") {

    MyVector<int> myVec{ 11, 22, 33, 44, 55, 66, 77, 88, 99 };
    REQUIRE(myVec.Size() == 9);

    MyVector<int> myVecCopy(5);

    myVecCopy = myVec;
    for (size_t i = 0; i < myVec.Size(); i++)
    {
        REQUIRE(myVecCopy[i] == myVec[i]);
    }
}


TEST_CASE("Erase element of vector", "[Modify]") {

    MyVector<int> myVec{ 11, 22, 33, 44, 55, 66, 77, 88, 99 };
    REQUIRE(myVec.Size() == 9);

    myVec.erase(2);
  
    for (size_t i = 0; i < myVec.Size(); i++)
    {
        REQUIRE(myVec[i] != 33);
    }
}

TEST_CASE("clear elements of vector", "[Modify]") {

    MyVector<int> myVec{ 11, 22, 33, 44, 55, 66, 77, 88, 99 };
    REQUIRE(myVec.Size() == 9);

    myVec.clear();

    REQUIRE(myVec.Size() == 0);
}

TEST_CASE("Vector of Vector", "Iterator")
{
	MyVector<int> myvec1{ 11,22,33,44,55,66,77,88,99 };
	MyVector<int> myvec2{ 110,220,330,440,550,660,770,880,990 };
	MyVector<int> myvec3{ 1100,2200,3300,4400,5500,6600,7700,8800,9900 };

	MyVector<int> finalVector{  11,22,33,44,55,66,77,88,99 , 
								110,220,330,440,550,660,770,880,990,
								1100,2200,3300,4400,5500,6600,7700,8800,9900 };

	MyVector<MyVector<int>> my2dVec{ myvec1, myvec2, myvec3 };

	MyVector<MyVector<int>>::Iterator itr = my2dVec.begin();

	int k = 0;
	for (size_t i = 0; i < my2dVec.Size(); i++)
	{
		MyVector<int> tempvec(*itr);

		MyVector<int>::Iterator tmpitr = tempvec.begin();
		for (size_t j = 0; j < tempvec.Size(); j++)
		{
			REQUIRE( *tmpitr == finalVector[k++]);
			++tmpitr;
		}
		++itr;
	}
}