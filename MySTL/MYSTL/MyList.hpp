#pragma once

template<typename valueType>
struct Node
{
  	valueType m_data;
    Node<valueType>* m_Next; // next pointer
    Node<valueType>* m_Prev; // previous pointer
};

template <typename T>
class MyList
{
public:
    MyList(); // default
    MyList(size_t n); // parametrized
    MyList(size_t n, T value); //parametrized
    

public:
    T operator[](size_t index); //index operator
private:
    Node<T>* m_Head;

};

template<typename T>
MyList<T>::MyList()
{
    m_Head = nullptr;
}

template<typename T>
MyList<T>::MyList(size_t n)
{
    if (n > 0)
    {
        m_Head = new Node<T>;
        m_Head->m_Next = nullptr;
        m_Head->m_Next = nullptr;
        //m_Head->m_data = T(); //new calls default ctr which calls all default ctr of members

        Node<T>* current = m_Head;

        Node<T>* temp = nullptr;
        for (size_t i = 1; i < n; i++)
        { 
            temp = new Node<T>;
            temp->m_Next = nullptr;
            temp->m_Next = nullptr;
            //temp->m_data = T(); no need to do this new does that

            current->m_Next = temp;
            temp->m_Prev = current;

            current = temp;
        }
    }
}

template<typename T>
MyList<T>::MyList(size_t n, T value)
{
    if (n > 0)
    {
        m_Head = new Node<T>;
        m_Head->m_Next = nullptr;
        m_Head->m_Next = nullptr;
        m_Head->m_data = value;

        Node<T>* current = m_Head;

        Node<T>* temp = nullptr;
        for (size_t i = 1; i < n; i++)
        {
            temp = new Node<T>;
            temp->m_Next = nullptr;
            temp->m_Next = nullptr;
            temp->m_data = value;

            current->m_Next = temp;
            temp->m_Prev = current;

            current = temp;
        }
    }
}

template <typename T>
T MyList<T>::operator[] (size_t index)
{
    Node<T>* itr = m_Head;
    
    for (size_t cnt = 0; cnt < index; cnt++)
    {
        itr = itr->m_Next;
    }

    return itr->m_data;

}








