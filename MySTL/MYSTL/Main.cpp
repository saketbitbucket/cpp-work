#include "stdio.h"
#include "MyVector.hpp"
#include "MyList.hpp"
#include <vector>
#include <iostream>
#include <algorithm>

struct Point
{
private:
    int x; int y; //screen pts

public:
    Point()
    {
        x = 11;
        y = 12;
    }

    Point(int x, int y)
    {
        this->x = x;
        this->y = y;
    }

    void Display()
    {
        std::cout << "pomt is " << x << "  " << y << std::endl;
    }
};

int main()
{
  
    /*MyList<int> lst(10);

    for (size_t i = 0; i < 10; i++)
    {
        std::cout << lst[i] << std::endl;

    }*/


    MyList<Point> lst(10, Point(11,22));

    for (size_t i = 0; i < 10; i++)
    {
        lst[i].Display();
    }
    

    return 0;
}