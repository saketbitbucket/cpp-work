
#pragma once
#include <iostream>
#include <initializer_list>


template<typename T>
class MyVector_Iterator
{
private:
	T* m_ptr;
public:

	MyVector_Iterator(T* ptr)
	{
		m_ptr = ptr;
	}

	MyVector_Iterator<T>& operator++() //pre increment
	{
		this->m_ptr++;
		return *this;
	}

	T operator*()
	{
		return *m_ptr;
	}

};

template<typename T>
class MyVector
{
public:
	typedef  MyVector_Iterator<T> Iterator;
private: 
	T* m_data;
	size_t m_size;
	size_t m_capacity;

	static const size_t factor = 2;

public:
	MyVector(); //default
	MyVector(size_t n); //parameterised
	MyVector(size_t n, T value); //parameterised
	MyVector(const MyVector<T>& other); //copy
	MyVector(MyVector<T>&& other); //Move constructor
	MyVector(std::initializer_list<T> list); //initiliser list

	MyVector<T>& operator=(const MyVector<T>& rhs); //copy - assignment operator
	MyVector<T>& operator=(MyVector<T>&& rhs); //move - assignment operator

	
	
	~MyVector();

public:
	size_t Size();
	size_t Capacity();
	T at(size_t index);
	void push_back(T value);
	T operator[](const size_t index);

	void clear();
	void erase(size_t index);
	bool isEmpty();

	Iterator begin()
	{
		return Iterator(m_data);
	}

	
private:
	size_t ReAlloc(size_t newSize);
};

template<typename T>
MyVector<T>::~MyVector()
{
	delete[] m_data;
}

template<typename T>
MyVector<T>::MyVector( MyVector<T>&& other)
{
	m_data = other.m_data;
	m_size = other.m_size;
	m_capacity = other.m_capacity;

	other.m_data = nullptr;
}


template<typename T>
MyVector<T>::MyVector()
{
	m_data = nullptr;
	m_size = 0;
	m_capacity = 0;
}

template<typename T>
MyVector<T>::MyVector(size_t n)
{
	m_data = new T[n];
	m_size = n;
	m_capacity = n;
}

template<typename T>
MyVector<T>::MyVector(size_t n, T value)
{
	m_data = new T[n];
	m_size = n;
	m_capacity = n;
	for (size_t i = 0; i < n; i++)
	{
		m_data[i] = value;
	}
}

template<typename T>
MyVector<T>::MyVector(const MyVector<T>& other)
{
	m_size = other.m_size;
	m_capacity = other.m_capacity;
	m_data = new T[m_capacity];

	for(size_t i=0; i< m_size;i++)
	    m_data[i] = other.m_data[i];
}

template <typename T>
MyVector<T>::MyVector(std::initializer_list<T> list)
{
	m_data = new T[list.size()];
	m_capacity = list.size();
	m_size = list.size();

	size_t i = 0;
	for (auto & element : list)
	{
		m_data[i++] = element;
	}
}

template<typename T>
MyVector<T>& MyVector<T>::operator=(const MyVector<T>& rhs)
{
	if (this != &rhs) // not same address of objects
	{
		delete[] m_data;

		m_size = rhs.m_size;
		m_capacity = rhs.m_capacity;
		m_data = new T[m_capacity];
		for (size_t i = 0; i < m_size; i++)
		{
			m_data[i] = rhs.m_data[i];
		}
	}

	return *this;
}


template<typename T>
MyVector<T>& MyVector<T>::operator=(MyVector<T>&& rhs)
{
	m_data = rhs.m_data;
	m_size = rhs.m_size;
	m_capacity = rhs.m_capacity;

	rhs.m_data = nullptr;

	return *this;  
}


template<typename T>
size_t MyVector<T>::Size()
{
	return m_size;
}

template<typename T>
size_t MyVector<T>::Capacity()
{
	return m_capacity;
}

template <typename T>
size_t MyVector<T>::ReAlloc(size_t newSize)
{
	T* newData = new T[newSize]; // using the new will call default ctor 
	                             // which will lead to 2 ctor call, dflt then cpy

	for (size_t i = 0; i < m_size; i++)
	{
		newData[i] = m_data[i]; // assignment opertaor
	}
	m_capacity = newSize;

	delete[] m_data;

	m_data = newData;

	return newSize;
}

template<typename T>
void MyVector<T>::push_back(T value)
{
	if (m_size >= m_capacity)
	{
		m_capacity = ReAlloc((m_capacity <1 ? 1:m_capacity)* factor);
	}
		
	m_data[m_size] = value;
	m_size++;
	
}

template<typename T>
T MyVector<T>::at(size_t index)
{
	return m_data[index];
}

template <typename T>
T MyVector<T>::operator[](const size_t index)
{
	return m_data[index];
}

template<typename T>
void MyVector<T>::clear()
{
	m_data = nullptr;
	m_size = 0;
	m_capacity = 0;
}

template<typename T>
void MyVector<T>::erase(size_t index)
{
	for (size_t i = index; i < m_size-1; i++)
	{
		m_data[i] = m_data[i + 1];
	}
	m_size--;

}

template<typename T>
bool MyVector<T>::isEmpty()
{
	m_size > 0 ? false : true;
}