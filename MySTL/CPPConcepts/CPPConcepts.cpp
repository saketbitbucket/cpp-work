// CPPConcepts.cpp : This file contains the 'main' function. Program execution begins and ends there.
//


//// Problem - Dynamicall allocate array of object
//// See if default constructor gets called...
//
// Cannot allocate array  of a object if class doesn't have default ctor
// Cannot use the class inside the std::Vector(n) , if no defalt ctor
// New calls the default ctor of the class for each object of array
// 
#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <map>

class Test
{
private :
    int i;
public:
    explicit Test(int val)
    {
        i = val;
    }

    Test()
    {
        i = 0;
    }

    Test(const Test& other)
    {
        i = other.i;
    }

    Test& operator = (const Test& rhs)
    {
        i = rhs.i;
        return *this;
    }

    void Display()
    {
        std::cout << i << std::endl;
    }
};

// Problem allocate array of object with new based on parameterized ctor == not possible
//int main()
//{
//    Test* objArray = new Test[10](-5); //error -- an array cn't be initilized with parameter
//    objArray[1].Display();
//}

// below program doesn't work if Test() = delete;
//int main()
//{
//    std::vector<Test> vec(10);
//
//    for (size_t i = 0; i < 10; i++)
//    {
//        vec[i].Display();
//    }
//}


//problem -- allocate memory with malloc and call assignment only once
//if we use new to allocate memory, first default constructor then assignment will be called

//int main()
//{
// 
//
//    //problem -- allocate memory with malloc and call assignment only once
//    // if we use new to allocate memory, first default constructor then assignment will be called
//    /*Test t1(11);
//
//    Test* ptr = (Test *)malloc(sizeof(Test));
//    *ptr = t1;
//    ptr->Display();*/
//    return 0;
//}

//
//int main()
//{
//    std::vector<Test> vec;
//	vec.emplace_back(11); // Create object with 11 and insert it -- uses placement new && operator new
//    vec.push_back(22); // error if ctor is explicit i.e no conversion allowed , otherwise call 2times ctor
//
//}

// Problem --- once you make a variable temporary abondon it bcz its state has deleted
//int main()
//{
//	std::vector<Test> vec;
//	vec.push_back(Test(11));
//	vec.push_back(Test(12));
//	vec.push_back(Test(13));
//	vec.push_back(Test(14));
//
//	std::vector<Test> vec1(std::move(vec));
//
//	for (size_t i = 0; i < 4; i++)
//	{
//		vec[i].Display();
//	}
//
//
//}

//problem -- demostrating initilizer list
//#include <vector>
//#include <initializer_list>
//
//int main()
//{
//    Test t1, t2, t3;
//    std::vector<Test> v1 { t1, t2, t3 }; 
//
//    std::vector<Test> v1{ t1, t2, t3, 1 }; //ERROR can't mix types bcz compiler 
//                                       //create temporary array of all the element in initi-list
//
//    std::initializer_list<int> lst {1, 2, 3, 4, 5};
//
//    auto size = lst.size();
//
//	return 0;
//}


//Problem -- Self assignment of a variable using move ctor, it is ignored to
// save the variable form getting invalidated..
//int main()
//{
//    std::vector<Test> vec;
//    vec.push_back(Test(11));
//    vec.push_back(Test(12));
//    vec.push_back(Test(13));
//    vec.push_back(Test(14));
//
//    // vec = std::move(vec);  //-- ignore seft assignment ans dnt invalidate vec
//
//    for (size_t i = 0; i < 4; i++)
//    {
//    	vec[i].Display();
//    }
//
//    return 0;
//}


//Problem - Implementation of the vector iterator class..
//int main()
//{
//    std::vector<Test> vec(2);
//    vec[1] = Test(1);
//
    std::vector<Test> vec1(2);
//    vec[1] = Test(1);
//
 
//
//    std::list<Test> lst;
//    std::list<Test>::iterator itr1 = lst.begin();;
//
//   //(vec == vec1);
//    return 0;
//}



//Problem - iterating vector of vector
//int main()
//{
//	std::vector<int> myvec1{ 11,22,33,44,55,66,77,88,99 };
//	std::vector<int> myvec2{ 110,220,330,440,550,660,770,880,990 };
//	std::vector<int> myvec3{ 1100,2200,3300,4400,5500,6600,7700,8800,9900 };
//
//
//	std::vector<std::vector<int>> my2dVec{ myvec1, myvec2, myvec3 };
//
//	std::vector<std::vector<int>>::iterator itr = my2dVec.begin();
//
//	for (size_t i = 0; i < my2dVec.size(); i++)
//	{
//		std::vector<int> tempvec(*itr);
//
//		std::vector<int>::iterator tmpitr = tempvec.begin();
//		for (size_t j = 0; j < tempvec.size(); j++)
//		{
//			std::cout << *tmpitr << "  ";
//			++tmpitr;
//		}		
//		++itr;
//		std::cout << std::endl;
//	}
//    
//    return 0;
//}


//// problem ---- virtual and delete
//using namespace std;
//
//class Animal
//{
//public:
//	virtual ~Animal() {
//		std::cout << "Base" << endl;
//	}
//	virtual void speak() {
//		cout << "basic" << endl;
//	}
//	virtual void run() { cout << "basic"; }
//};
//
//
//
//class Dog : public Animal
//{
//public:
//	void run() = delete;
//public:
//	void speak() { cout << "woof" << endl; }
//};
//int main()
//{
//	Animal * pb = new Dog();
//	pb->speak();
//	pb->run();
//
//
//
//}

// virtual and delete
using namespace std;

class Animal
{
public:
	virtual ~Animal() {
		std::cout << "Base" << endl;
	}
	virtual void speak() {
		cout << "basic" << endl;
	}
	virtual void run() { cout << "basic"; }
};



class Dog : public Animal
{
public:
	void run() {};
private:
	void speak() { cout << "dog ---->woof" << endl; }
};
int main()
{
	Animal * pb = new Dog();
	pb->speak();
}